###
### MATSUO & TSUMURA lab. 
###   ~/.bash_profile template
###  feel free to edit this file at your own risk
###
### Last Modified: 2011/05/06 13:00
### Created:       2007/04/03 17:50


# exec zsh 
if `which zsh > /dev/null 2>&1`; then
	[ -n "$PS1" -a -z "$RUN_BASH" ] && exec zsh
fi

#
# environment variables
#
export PS1="\u@\h:\w/\$ "
export PAGER="less"
export LESS="-imqMXR -x 8"
# export LESSCOLOR=red
export EDITOR="vi"
#
export TEXMFHOME=~/texmf	# dont set TEXINPUTS
export BIBCITEDIR=${TEXMFHOME}/bibtex/bib
# export CVSROOT=/project/camp/cvs  # for camp group
export CVS_RSH=ssh

#
# aliases
#
alias ls='ls -F'
alias la='ls -A'
alias ll='ls -l'
alias du='du -hk'
alias rdesktop='rdesktop -g 1920x1000'
alias mv='mv -i'
set -o noclobber		# no overwrite when redirect

# user file-creation mask
umask 022
ulimit -c 0

#
# PATH
#
case $OSTYPE in
linux-gnu*)
    export PATH=/bin:/usr/bin:/usr/X11R6/bin:/usr/local/bin:${HOME}/bin
#    echo -n '[31m'
#    /usr/bin/quota -q
#    echo -n '[00m'
    ;;
solaris*)
    export LANG='ja'
    export MANPATH=/usr/man:/opt/local/man:/usr/sfw/man:/opt/csw/man:/opt/SUNWspro/man:/usr/openwin/man:/usr/dt/man:/usr/X11/man
    export PATH=/opt/local/bin:/usr/bin:/usr/sfw/bin:/opt/csw/bin:/usr/ccs/bin:/opt/SUNWspro/bin:/usr/openwin/bin:/usr/dt/bin:/usr/X11/bin
#    stty erase ^h
#    stty intr  ^c
#    stty susp  ^z
#    echo -n '[31m'
#    /usr/sbin/quota
#    echo -n '[00m'
    ;;
esac

