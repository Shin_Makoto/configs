#!/bin/bash

PWD=`pwd`

ln -nsf ${PWD}/.bashrc ${HOME}/.bashrc
ln -nsf ${PWD}/.zshrc ${HOME}/.zshrc
ln -nsf ${PWD}/.tmux.conf ${HOME}/.tmux.conf
