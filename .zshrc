###
### MATSUO & TSUMURA lab. 
###   ~/.bash_profile template
###  feel free to edit this file at your own risk
###
### Last Modified: 2008/01/07 16:30
### Created:       2007/04/03 17:50

#
# environment variables
#
#export PS1="\u@\h:\w/\$ "
#export PAGER="less"
export LESS="-imqMXR -x 8"
#setopt prompt_subst
autoload colors
#PROMPT="%n@%m%% "
PROMPT="$USER@%m%% "
RPROMPT="[%~]"
SPROMPT="correct: %R -> %r ? "

# export LESSCOLOR=red
export EDITOR="vi"

export TEXMFHOME=~/texmf	# dont set TEXINPUTS
export BIBCITEDIR=${TEXMFHOME}/bibtex/bib
export CVSROOT=/home/muramatsu/cvsroot  # for camp group
export CVS_RSH=ssh
export LD_LIBRARY_PATH=/usr/local/gcc42-0715-32/lib:/opt/gcc/alpha/4.5.3/lib
# export CVSROOT=/project/camp/cvs  # for camp group
export CVS_RSH=ssh

#
# aliases
#
alias bash='RUN_BASH="run" bash'
alias ls='ls -F'
alias la='ls -A'
alias ll='ls -l'
alias du='du -hk'
alias -g G='| grep'
alias -g L='| less'
alias basilic='rdesktop -x l -g 1920x1000 basilic'
alias basilic19='rdesktop -x l -g 1280x980 basilic'
alias onion='rdesktop -x -l -g 1920x1000 onion'
alias onion19='rdesktop -x -l -g 1280x980 onion'
#set -o noclobber		# no overwrite when redirect

# user file-creation mask
umask 022

# zsh-history configure
HISTFILE=${HOME}/.zsh-history
HISTSIZE=100000
SAVEHIST=100000
setopt extended_history
# function_history-all { history -E 1 }
#
# zsh gadgets
#
autoload -U compinit
compinit

# 補完で小文字でも大文字にマッチさせる
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

bindkey -e
bindkey ' ' magic-space

setopt always_last_prompt
setopt auto_cd
setopt auto_list
setopt auto_param_slash
setopt auto_pushd
setopt auto_remove_slash
setopt auto_resume
setopt bad_pattern
setopt beep
setopt brace_ccl
setopt pushd_silent
setopt complete_aliases
setopt complete_in_word
setopt correct_all
setopt csh_null_glob
setopt extended_glob
unsetopt flow_control           # disable stop=^s and start=^q
setopt glob_dots
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_reduce_blanks
setopt interactive_comments
setopt list_ambiguous
setopt list_types
setopt path_dirs
setopt pushd_ignore_dups
setopt pushd_to_home
setopt short_loops
setopt sun_keyboard_hack
setopt rc_expand_param

limit -h coredumpsize 0

#
# PATH
#
case $OSTYPE in
linux-gnu*)
    export PATH=/bin:/usr/bin:/sbin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/texlive/2012/bin/x86_64-linux:/usr/libexec:~/bin
    echo -n '[31m'
#    /usr/bin/quota -q
    echo -n '[00m'
    ;;
solaris*)
    export LANG='ja'
    export MANPATH=/usr/man:/opt/local/man:/usr/sfw/man:/opt/csw/man:/opt/SUNWspro/man:/usr/openwin/man:/usr/dt/man:/usr/X11/man
    export PATH=/opt/local/bin:/usr/bin:/usr/sfw/bin:/opt/csw/bin:/usr/ccs/bin:/opt/SUNWspro/bin:/usr/openwin/bin:/usr/dt/bin:/usr/X11/bin
    stty erase ^h
    stty intr  ^c
    stty susp  ^z
    echo -n '[31m'
    /usr/sbin/quota
    echo -n '[00m'
    ;;
esac

# 履歴検索機能のショートカット
autoload history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^P" history-beginning-search-backward-end
bindkey "^N" history-beginning-search-forward-end

